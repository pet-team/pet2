#! /usr/bin/perl

use strict;
use warnings;

use CGI::Simple;
use Debian::PET::Cache;
use Debian::PET::Config qw( config );
use Template;

my $q     = CGI::Simple->new;
my $cache = Debian::PET::Cache->new(directory => config->{cache})->read("changelog");

my $package = $q->param("p");
if (! defined $package) {
  print $q->header(-status => '400 Bad Request', type => 'text/plain');
  print "Bad Request: No package specified.\n";
  exit;
}
if (! exists $cache->{ $package }) {
  print $q->header(-status => '404 Not Found', type => 'text/plain');
  print "Not found.\n";
  exit;
}

my $changelog = $cache->{ $package };
print $q->header(-type => "text/html", -charset => "utf-8");

my $text = $changelog->{changelog};

# TODO: display correct changelog for tagged versions...
my $rel = $q->param("tag") ? 'rel' : 'unrel';

# stolen from old pet:
# pet-chlog.cgi:
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Released under the terms of the GNU GPL 2

$text =~ s/&/&amp;/g;
$text =~ s/'/&apos;/g;
$text =~ s/"/&quot;/g;
$text =~ s/</&lt;/g;
$text =~ s/>/&gt;/g;
$text =~ s{\r?\n}{<br/>}g;

# replace bug-numbers with links
$text =~ s{
    (               # leading
        ^           # start of string
        |\W         # or non-word
    )
    \#(\d+)         # followed by a bug ID
    \b              # word boundary
}
{$1<a href="http://bugs.debian.org/$2">#$2</a>}xgm;
# treat text as multi-line
# Same for CPAN's RT
$text =~ s{\bCPAN#(\d+)\b}
{<a href="http://rt.cpan.org/Ticket/Display.html?id=$1">CPAN#$1</a>}gm;

print qq(<a style="float: right; margin: 0 0 1pt 1pt; clear: none;"
    href="javascript:async_get( '${package}_${rel}_chlog_balloon',
        'pet-chlog.cgi?pkg=$package;rel=$rel')">reload</a>\n);
print qq(<code style="white-space: pre">$text</code>);

# vim:set et sw=2:
