#! /usr/bin/perl

use strict;
use warnings;

use CGI::Simple;
use Debian::PET::Cache;
use Debian::PET::Config qw( config );
use JSON;

my $q = CGI::Simple->new;

my $cache_name = $q->param("cache") || "consolidated";
my $type       = $q->param("type");
my @keys       = $q->param("key");

if ($cache_name !~ /^[a-zA-Z0-9]+$/) {
  print $q->header(-status => "400 Bad Request", -type => "text/plain");
  print "Bad Request: Invalid cache name given.\n";
  exit;
}

my $cache;
if (defined $type && $type eq "shared") {
  $cache = Debian::PET::Cache->new(directory => config->{cache_shared});
}
elsif (! defined $type) {
  $cache = Debian::PET::Cache->new(directory => config->{cache});
}
else {
  print $q->header(-status => "400 Bad Request", -type => "text/plain");
  print "Bad Request: Invalid type given.\n";
  exit;
}

print $q->header(-type => "application/json");
$cache->dump($cache_name, \*STDOUT, @keys);

# vim:set et sw=2:
