#! /usr/bin/perl

use strict;
use warnings;

use CGI::Simple;
use Debian::PET::Cache;
use Debian::PET::Config qw( config );
use Template;



my $q = CGI::Simple->new;
my $cache = Debian::PET::Cache->new(directory => config->{cache});

my $template_dir = config->{templates};
unless (defined $template_dir) {
  require File::ShareDir
      or die "Option 'templates' not set and could not load File::ShareDir to locale system-wide version.'";
  require File::Spec;

  $template_dir = File::Spec->catdir(File::ShareDir::dist_dir("Debian-PET"), "templates");
}

my $template = Template->new({
  INCLUDE_PATH => $template_dir,
  INTERPOLATE  => 1,
  FILTERS      => {
    quotemeta => sub { quotemeta(@_); },
  },

  PRE_CHOMP    => 2,
  POST_CHOMP   => 2,
  TRIM         => 1,
});



my $archive = $q->param("archive") || "debian";
my $bugs    = $q->param("bugs")    || "debian";

my $data = $cache->read("consolidated");
my @packages = keys %$data;

my %packages_by_section;
for my $package (@packages) {
  my %p = %{ $data->{ $package } || {} };
  my $p_archive = $p{archive}->{ $archive };

  my $section =
    $p{vcs}->{ready_for_upload} && ! $p_archive->{trunk_in_archive} ? 'ready_for_upload' :
    $p{bugs}->{debian}->{has_rc_bugs}        ? 'rc_buggy'         :
    $p{vcs}->{work_in_progress} && $p{vcs}->{no_tags} ? 'wip_new' :
    $p{watch}->{upstream_higher}             ? 'new_upstream'     :
    $p_archive->{tag_is_higher}             ? 'tagged_not_archive' :
    $p_archive->{tag_is_lower}              ? 'strange_version'  :
    $p_archive->{trunk_is_lower}            ? 'strange_version'  :
    $p{vcs}->{ignored}                       ? 'ignored'          :
    $p{vcs}->{work_in_progress}              ? 'work_in_progress' :
    $p{watch}->{upstream_lower}              ? 'strange_version'  :
#    $f{ready_for_upload} ? 'ready_for_upload' :
#    $p{archive_newer}    ? 'strange_version'  :
#    $f{work_in_progess}  ? 'work_in_progress' :
    'other';

  push @{ $packages_by_section{ $section } ||= [] }, $package;
}



my $vars = {
  # the great hash
  data => $data,

  # source selectors
  archive => $archive,
  bugs    => $bugs,

  # sorting, selection, ...
  packages_by_section => \%packages_by_section,
};

print $q->header(-type => "text/html", -charset => "utf-8");
$template->process("overview", $vars)
  or die $template->error(), "\n";

exit 0;

# vim:set et sw=2:
