#! /usr/bin/perl

use strict;
use warnings;

use CGI::Simple;
use Debian::PET::Config qw( config );
use File::Spec;

my $q = CGI::Simple->new;

unless (defined config->{requests} && -d config->{requests}) {
  server_error("Configuration option 'requests' not set or not a directory.");
}

unless ($q->request_method eq 'POST') {
  client_error("Only POST requests are supported.");
}

my $action  = $q->param("action");

if ($action eq "notify") {
  action_notify();
}

client_error("Unknown action: $action");
exit 1;

###########################################################

sub action_notify {
  my $vcs = $q->param("repository")
    or client_error("The 'name' parameter is required for notifications.");

  unless (exists config->{vcs}->{ $vcs }) {
    client_error("Unknown VCS: $vcs");
  }

  open my $fh, ">", File::Spec->catfile(config->{requests}, "update-$vcs")
    or server_error("Could not store request.");
  print $fh "requested-by: $ENV{REMOTE_ADDR}\n";
  close $fh;

  ok("Ok. Have fun.");
  exit 0;
}

###########################################################

sub client_error {
  print STDERR @_, "\n";

  print $q->header(
    -status => '400 Bad Request',
    -type   => 'text/plain',
  );
  print @_, "\n";

  exit 1;
}

sub server_error {
  print STDERR @_, "\n";

  print $q->header(
    -status => '500 Internal Server Error',
    -type   => 'text/plain',
  );
  print @_, "\n";

  exit 1;
}

sub ok {
  my @message = @_;
  @message = "OK." unless @message;

  print $q->header(
    -status => '200 OK',
    -type   => 'text/plain',
  );
  print @message, "\n";
  
  exit 0;
}

# vim:set et sw=2:
