#! /usr/bin/perl

use strict;
use warnings;

use Test::More 0.88;

use X::Log4perl;

BEGIN { use_ok("Debian::PET::VCS::Subversion"); }

my $svn = Debian::PET::VCS::Subversion->new(
  url => 'svn://svn.debian.org/pkg-perl',
);

ok(
  $svn->get_file(
    package => 'libhtml-formfu-perl',
    file    => 'debian/compat',
  ),
  "get a file from trunk"
);

ok(
  $svn->get_file(
    package => 'libnamespace-clean-perl',
    file    => 'debian/compat',
    branch  => 'lenny',
  ),
  "get a file from a branch"
);

ok(
  $svn->get_file(
    package => 'libhtml-formfu-perl',
    file    => 'debian/compat',
    tag     => '0.07001-1',
  ),
  "get a file from a tagged version"
);

done_testing;

# vim:set et sw=2:
