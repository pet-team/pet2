package
    X::Log4perl;

use Log::Log4perl;

Log::Log4perl->init(\*DATA);

1;

__DATA__

log4perl.rootLogger = DEBUG, Screen

log4perl.appender.Screen = Log::Log4perl::Appender::Screen
log4perl.appender.Screen.layout = PatternLayout
log4perl.appender.Screen.layout.ConversionPattern = # %c: %m%n
