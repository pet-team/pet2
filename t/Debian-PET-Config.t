#! /usr/bin/perl

use strict;
use warnings;

use Test::More 0.88;
use Test::Exception;

BEGIN { use_ok("Debian::PET::Config"); }

{
  local $ENV{PET_CONFIG};
  delete $ENV{PET_CONFIG};
  dies_ok { Debian::PET::Config::config() } "dies when no config is set";
}

{
  local $ENV{PET_CONFIG} = "/does/not/exist";
  dies_ok { Debian::PET::Config::config() } "dies on non-existing config";
}

{
  local $ENV{PET_CONFIG} = "t/data/test.conf";

  my $config;
  lives_ok { $config = Debian::PET::Config::config() } "can get config";
  ok($config, '$config is defined');
  is($config->{foo}, "bar", "foo is set to bar");
}

done_testing();

# vim:set et sw=2:
