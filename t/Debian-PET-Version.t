#! /usr/bin/perl

use strict;
use warnings;

use Test::More 0.88;

BEGIN { use_ok("Debian::PET::Version", qw(max_version min_version)); }

{
  my @vs = qw( 1.2-1 1.2-2 2.0-1 );
  is(max_version(@vs), "2.0-1", "highest version is 2.0-1");
  is(min_version(@vs), "1.2-1", "lowest version is 1.2-1");
}

{
  my @vs = qw( 2.0-1 1.2-2 1.2-1 );
  is(max_version(@vs), "2.0-1", "highest version is 2.0-1");
  is(min_version(@vs), "1.2-1", "lowest version is 1.2-1");
}

{
  my @vs = qw( 1.1-1 1.0-1 1.3-1 1.2-1 );
  is(max_version(@vs), "1.3-1", "highest version is 1.3-1");
  is(min_version(@vs), "1.0-1", "lowest version is 1.0-1");
}

done_testing;

# vim:set et sw=2:
