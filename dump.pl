#! /usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use Debian::PET::Cache;
use Debian::PET::Config qw( config );

my $cache = Debian::PET::Cache->new(directory => config->{cache});
my $name = shift @ARGV;
$cache->dump($name, \*STDOUT, @ARGV);
