package Debian::PET::Queues;

use Moose;
use namespace::autoclean;

use Dpkg::Version;
use Log::Log4perl;
use LWP::Simple;
use Parse::DebControl;

has 'url' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'http://ftp-master.debian.org/new.822',
);

has '_cache' => (
  is      => 'rw',
  isa     => 'ArrayRef[HashRef[Str]]',
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub update {
  my ($self) = @_;

  $log->debug("Downloading " . $self->url);

  my $content = get($self->url)
                or $log->logconfess("Downloading " . $self->url . " failed.");
  my $parser  = Parse::DebControl->new;
  $self->_cache($parser->parse_mem($content));
}

sub queue {
  my ($self, $queue_name) = @_;

  my %packages;

  $self->update unless defined $self->_cache;
  for (@{ $self->_cache }) {
    next unless $_->{Queue} eq $queue_name;
    next if exists $packages{ $_->{Source} }
            && version_compare_relation($_->{Version}, '=<', $packages{ $_->{Source} }->{version}); 

    $packages{ $_->{Source} } = {
      source  => $_->{Source},
      version => $_->{Version},
    };
  }

  return \%packages;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::Queues - access to various queues

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
