package Debian::PET::Gatherer::VCS;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use Debian::PET;
use Debian::PET::Config qw( config );
use Debian::PET::Version qw( max_version );
use List::MoreUtils qw( any );
use Log::Log4perl;
use Parse::DebianChangelog;
use Try::Tiny;

has 'name' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'VCS',
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _vcs {
  my $self = shift;
  Debian::PET->instance->vcs(@_);
}

sub cache {
  Debian::PET->instance->cache;
}

# merge information from Debian::PET::VCS->changed_packages
sub _merge_diff {
  my ($self, $status, $diff) = @_;

  while (my ($name, $new) = each %$diff) {

    my $old = $status->{ $name } ||= {};

    $old->{vcs} = $diff->{vcs} if exists $diff->{vcs};

    if (exists $new->{trunk}) {
      if (defined $new->{trunk}) {
        $old->{trunk} = $new->{trunk};
      }
      else {
        delete $status->{ $name };
        $log->debug("Package $name has been removed from VCS, purged from cache.");
        next;
      }
    }

    for my $type ("branches", "tags") {

      # 'branches' and 'tags' should exist even when they are empty
      $old->{ $type } ||= {};

      while (my ($n, $v) = each %{ $new->{ $type } }) {
        if (defined $v) {
          $old->{ $type }->{ $n } = $v;
        }
        else {
          delete $old->{ $type }->{ $n };
        }
      }

    }

  }
}

sub gather {
  my ($self, %p) = validated_hash(\@_,
    packages     => { isa => 'HashRef', optional => 1 },
    all_packages => { isa => 'ArrayRef', },
    force        => { isa => 'Bool', default => 0 },
    force_all    => { isa => 'Bool', default => 0 },
  );

  return unless exists $p{packages};
  $log->info("Gathering VCS information for ", scalar keys %{ $p{packages} }, " packages");

  my $c_vcs = $self->cache->read("vcs");
  $self->_merge_diff($c_vcs, $p{packages});

  while (my ($package, $diff) = each %{ $p{packages} }) {

    if (exists $diff->{trunk} && ! defined $diff->{trunk}) {
      # package has been removed on this run
      next;
    }

    my $vcs_name = $diff->{vcs} || $self->_get_vcs_for_package($package) or next;

    my $vcs = $self->_vcs($vcs_name);
    unless (defined $vcs) {
      $log->error("Package '$package' lives in unknown VCS '$vcs_name'");
      next;
    }

    try {
      $self->update_package(
        package  => $package,
        diff     => $diff,
        vcs_name => $vcs_name,
        force    => $p{force},
      );
    }
    catch {
      $log->error("Error while updating VCS information for $package: $_");
    }

  }
}

sub _get_vcs_for_package {
  my ($self, $package) = @_;

  my $vcs = $self->cache->read("vcs")->{ $package }->{vcs};
  return $vcs if defined $vcs;

  my @vcses;
  for my $name (Debian::PET->instance->vcses) {
    my $vcs = $self->_vcs($name);
    if (any { $_ eq $package } $vcs->packages) {
      push @vcses, $name;
    }
  }

  if (@vcses == 0) {
    $log->error("Could not find $package in any VCS.");
  }
  elsif (@vcses > 1) {
    $log->error("Package $package found in several VCS: ", join(", ", @vcses));
  }

  return wantarray ? @vcses : $vcses[0];
}

# XXX: make private?
sub update_package {
  my ($self, %p) = validated_hash(\@_,
    package  => { isa => 'Str' },
    diff     => { isa => 'HashRef' },
    vcs_name => { isa => 'Str' },
    force    => { isa => 'Bool', default => 0 },
  );

  $log->debug("Updating package ", $p{package}, " from ", $p{vcs_name});

  # XXX: move to calling function?
  my $c_changelog = $self->cache->read("changelog");
  my $c_control   = $self->cache->read("control");
  my $c_vcs       = $self->cache->read("vcs");
  my $c_watch     = $self->cache->read("watch_file");

  my $now = time;

  my $vcs_status = $c_vcs->{ $p{package} } ||= {};

  my $vcs = $self->_vcs($p{vcs_name});
  unless ($vcs) {
    $log->error("Package $p{package} lives in unknown VCS '$p{vcs_name}'");
    return 1;
  }

  if ($p{force} || $p{diff}->{trunk}) {
    my $vcs_changelog = $self->_vcs_changelog($p{package}, $vcs);

    $c_changelog->{ $p{package} }->{changelog} = $vcs_changelog->{Changes};
    $c_changelog->{ $p{package} }->{_pet_timestamp} = $now;
    $c_control->{ $p{package} }->{control} = $vcs->get_file(package => $p{package}, file => "debian/control");
    $c_control->{ $p{package} }->{_pet_timestamp} = $now;
    $c_watch->{ $p{package} }->{watch} = $vcs->get_file(package => $p{package}, file => "debian/watch");
    $c_watch->{ $p{package} }->{_pet_timestamp} = $now;

    $vcs_status->{waits} = _waits_for($vcs_changelog->{Changes});
    $vcs_status->{ignore_version} = _ignore_version($vcs_changelog->{Changes});

    my $series = $vcs->get_file(package => $p{package}, file => "debian/patches/series");
    if (defined $series) {
      my @patches = $series =~ /^([^#].*)$/gm;
      # TODO: should be HashRef including links, maybe also forwarded etc.
      $vcs_status->{patches} = \@patches;
    }
    else {
      $vcs_status->{patches} = [];
    }

    for my $field qw(Source Version Distribution Urgency Maintainer Date) {
      $vcs_status->{lc $field} = $vcs_changelog->{$field};
    }
    $vcs_status->{closes} = [ split / +/, $vcs_changelog->{Closes} ];
  }

  if ($p{force}) {
    $vcs_status->{tags}  = [ $vcs->tags(package => $p{package}) ];
    $vcs_status->{trunk} = $vcs->trunk(package => $p{package});
    # TODO: add support for branches
  }

  $vcs_status->{highest_version} = max_version keys %{ $vcs_status->{tags} };
  $vcs_status->{vcs} = $p{vcs_name};
  $vcs_status->{_pet_timestamp} = $now;

  return 0;
}

sub _vcs_changelog {
  my ($self, $package, $vcs) = @_;

  my $changelog = $vcs->get_file(
    package => $package,
    file    => "debian/changelog",
  );

  my $cl = Parse::DebianChangelog->init;
  $cl->parse({ instring => $changelog });
  return scalar $cl->dpkg;
}

sub _waits_for {
  my $changes = shift;
  my %waits = $changes =~ /^\s+WAITS[ -]FOR:\s*(\S+)(?:\s+(}S+))?/igm or return {};

  for (values %waits) {
    defined $_ or $_ = '~';
  }

  return \%waits;
}

sub _ignore_version {
  my $changes = shift;
  my @ignores = $changes =~ /^\s+IGNORE[ -]VERSION:\s*(\S+)/igm or return [];
  return \@ignores;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::Gatherer::VCS - gather information from VCS

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
