package Debian::PET::Gatherer::Watch;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use Debian::PET;
use Debian::PET::SafeReplace qw( safe_replace );
use Debian::PET::UserAgent;
use Debian::PET::Version qw( max_version );
use Debian::PET::Watch qw( parse_watch );
use Dpkg::Version qw( version_check version_compare );
use List::MoreUtils qw( all any );
use Log::Log4perl;
use URI;

has 'timeout' => (
  is      => 'rw',
  isa     => 'Int',
  default => 7200,
);

has 'cpan' => (
  is      => 'rw',
  isa     => 'Str',
);

has 'name' => (
  is       => 'rw',
  isa      => 'Str',
  required => 1,
);

has '_agent' => (
  is      => 'ro',
  isa     => 'Debian::PET::UserAgent',
  lazy    => 1,
  builder => '_agent_builder',
);

has '_cpan' => (
  is       => 'ro',
  isa      => 'Maybe[Debian::PET::SharedData::CPAN]',
  lazy     => 1,
  builder  => '_cpan_builder',
  init_arg => undef,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _agent_builder {
  Debian::PET::UserAgent->new;
}

sub _cpan_builder {
  my $self = shift;
  if (defined $self->cpan) {
    return Debian::PET->instance->shared_data($self->cpan);
  }
  return undef;
}

sub cache {
  Debian::PET->instance->cache;
}

sub gather {
  my ($self, %p) = validated_hash(\@_,
    packages     => { isa => 'HashRef', optional => 1 },
    all_packages => { isa => 'ArrayRef', optional => 1 },
    force        => { isa => 'Bool', default => 0 },
    force_all    => { isa => 'Bool', default => 0 },
  );

  my @packages;

  if (! exists $p{packages} || $p{force_all}) {
    unless ($p{all_packages}) {
      $log->error("Updated forced, but all_packages not given.");
      return;
    }
    @packages = @{ $p{all_packages} };

    $log->info("Checking watches for all ", scalar @packages, " packages (forced)");
  }
  elsif ($p{force}) {

    @packages = grep { defined $p{packages}->{ $_ }{trunk} } keys %{ $p{packages} };

  }
  else {

    # update only packages where trunk has changed
    while (my ($name, $status) = each %{ $p{packages} }) {
      defined $status->{trunk} or next;
      push @packages, $name;
    }

    $log->info("Checking watches for ", scalar @packages, " packages");
  }

  for my $package (@packages) {
    $self->_check_package($package, $p{force});
  }
}

sub _check_package {
  my ($self, $package, $force) = @_;

  my $c_watch_file     = $self->cache->read("watch_file");
  my $c_watch_upstream = $self->cache->read("watch");
  my $c_vcs            = $self->cache->read("vcs");

  return unless defined $c_watch_file->{ $package }->{watch};

  my $timestamp = $c_watch_upstream->{$package}->{_pet_timestamp};
  return if ! $force && defined($timestamp) && $timestamp + $self->timeout > time;

  $log->debug("Checking watches for $package...");

  my $vcs_version;
  if (exists $c_vcs->{ $package }->{version}) {
    $vcs_version = $c_vcs->{ $package }->{version};
  }
  else {
    $log->warn("VCS version for $package is unknown");
    $vcs_version = "~~UNKNOWN~~";
  }

  my $watches = parse_watch($c_watch_file->{ $package }->{watch});
  unless (defined $watches) {
    $log->error("Invalid watch file for $package");
    $c_watch_upstream->{ $package } = {
      errors         => [ "InvalidWatch" ],
      _pet_timestamp => time,
    };
    return;
  }

  my @results;
  for my $watch (@{ $watches->{watches} }) {
    my ($error, $versions) = $self->_check_watch($package, $watch);

    if (defined $error) {
      push @results, { error => $error };
      next;
    }

    my $upstream_mangled;
    if (my $rules = $watch->{options}->{uversionmangle} || $watch->{options}->{versionmangle}) {
      while (my ($version, $download_link) = each %$versions) {
        my $mangled_version = $version;
        safe_replace(\$mangled_version, $_) for @$rules;
        $upstream_mangled->{ $mangled_version } = $download_link;
      }
    }
    else {
      $upstream_mangled = $versions;
    }

    my $vcs_mangled = $vcs_version;
    $vcs_mangled =~ s/^\d+://;
    $vcs_mangled =~ s/-[a-zA-Z0-9+.~]*$//;
    if (my $rules = $watch->{options}->{dversionmangle} || $watch->{options}->{versionmangle}) {
      safe_replace(\$vcs_mangled, $_) for @$rules;
    }

    unless (all { version_check($_) } keys %$upstream_mangled, $vcs_mangled) {
      push @results, { error => "InvalidVersion" };
      next;
    }

    my $upstream_highest = max_version(keys %$upstream_mangled);

    my %watch_result = (
      upstream_highest  => $upstream_highest,
      vcs_mangled       => $vcs_mangled,
      download_link     => $upstream_mangled->{ $upstream_highest },
      link              => $watch->{homepage},

      # debug
      watch             => $watch,
      upstream_versions => $versions,
      upstream_mangled  => $upstream_mangled,
    );

    push @results, \%watch_result;
  }

  my $highest = _highest_watch_result(@results) || {};

  my $upstream_higher = undef;
  my $upstream_lower  = undef;
  if (defined $highest->{upstream_highest}) {
    $upstream_higher = version_compare($highest->{upstream_highest}, $highest->{vcs_mangled}) > 0;
    $upstream_lower  = version_compare($highest->{upstream_highest}, $highest->{vcs_mangled}) < 0;
  }

  $c_watch_upstream->{ $package } = {
    versions         => \@results,
    upstream_highest => $highest->{upstream_highest},
    upstream_higher  => $upstream_higher,
    upstream_lower   => $upstream_lower,
    link             => $highest->{link},
    download_link    => $highest->{download_link},
    errors           => [ map { $_->{error} || () } @results ],
    _pet_timestamp   => time,
  };
}

sub _highest_watch_result {
  my $highest;
  for my $result (@_) {
    next unless defined $result->{upstream_highest};
    if (! defined $highest || version_compare($result->{upstream_highest}, $highest->{upstream_highest}) > 0) {
      $highest = $result;
    }
  }
  return $highest;
}

sub _check_watch {
  my ($self, $package, $watch) = @_;

  my $versions = {};
  my $homepage = $watch->{homepage};

  if (defined $self->_cpan && $homepage =~ m{^http://search.cpan.org}) {
    $log->debug("Querying CPAN for $homepage");
    (undef, $versions) = $self->_cpan->watch($homepage, qr/^$watch->{pattern}$/);
    return (undef, $versions) if defined $versions && %$versions;
  }
  $log->debug("$homepage not found on CPAN, trying on my own");

  $homepage =~ s{^http://sf\.net/}{http://qa.debian.org/watch/sf.php/};

  my $result = $self->_agent->get($homepage);
  return "DownloadError" unless $result->is_success;
  my $content = $result->decoded_content;

  my @links;
  if ($homepage =~ m{^https?://}) {
    @links = $content =~ m{href=(?:"([^"]+)"|'([^']+)'|([^'">]+))}gi;
  }
  else {
    @links = split /\s+/, $content;
  }

  for my $link (grep defined $_, @links) {
    my @parts = $link =~ /^$watch->{pattern}$/ or next;
    my $download_link = URI->new_abs($link, $homepage);
    my $version = join(".", @parts);
    $versions->{ $version } = "$download_link";
  }

  return "NotFound" unless %$versions;
  return (undef, $versions);
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::Gatherer::Watch - watch new upstream releases

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
