package Debian::PET::Gatherer::BTS;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use Debian::PET;
use List::MoreUtils qw( uniq );
use Log::Log4perl;
use SOAP::Lite;

has 'soap_uri' => (
  is      => 'ro',
  isa     => 'Str',
  default => 'Debbugs/SOAP',
);

has 'soap_proxy' => (
  is      => 'ro',
  isa     => 'Str',
  default => 'http://bugs.debian.org/cgi-bin/soap.cgi',
);

has 'bug_link_base' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'http://bugs.debian.org/',
);

has 'package_link_base' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'http://bugs.debian.org/src:',
);

has 'name' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'bugs.debian',
);

has 'timeout' => (
  is      => 'rw',
  isa     => 'Int',
  default => 3600,
);

has '_bts' => (
  is      => 'ro',
  lazy    => 1,
  builder => '_bts_builder',
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub cache {
  Debian::PET->instance->cache;
}

sub _bts_builder {
  my $self = shift;
  SOAP::Lite->uri($self->soap_uri)->proxy($self->soap_proxy);
}

sub _update_bugs {
  my ($self, @bugs) = @_;

  return unless @bugs;
  $log->debug("Updating information on ", scalar @bugs, " bugs");

  my $bugs  = $self->_bts->get_status(@bugs)->result();
  unless ($bugs) {
    $log->error("Could not get bug status from BTS.");
    return;
  }

  my $cache = $self->cache->read($self->name);
  my $now   = time;

  for my $bug (values %$bugs) {

    my $bugnum   = $bug->{bug_num};
    my @packages = split /, /, $bug->{source};
    unless (@packages) {
      $log->error("no source package(s) for bug #", $bugnum);
      next;
    }

    my %info = (
      _pet_timestamp => $now,

      done      => $bug->{pending} eq 'done',
      forwarded => $bug->{forwarded},
      owner     => $bug->{owner},
      pending   => $bug->{pending} eq 'pending',
      severity  => $bug->{severity},
      tags      => [ split /\s+/, $bug->{tags} ],
      title     => $bug->{subject},

      link      => $self->bug_link_base . $bugnum,
    );

    for my $package (@packages) {
      if ($info{done}) {
        delete $cache->{ $package }->{bugs}->{ $bugnum };
      }
      else {
        $cache->{ $package }->{bugs}->{ $bugnum } = \%info;
      }
    }

  }
}

sub gather {
  my ($self, %p) = validated_hash(\@_,
    packages     => { isa => 'HashRef', optional => 1 },
    all_packages => { isa => 'ArrayRef' },
    force        => { isa => 'Bool', default => 0 },
    force_all    => { isa => 'Bool', default => 0 },
  );

  my $now = time;
  my $c_bts = $self->cache->read($self->name);

  my @packages;
  if (! exists $p{packages} || $p{force_all}) {

    @packages = @{ $p{all_packages} };

    # When we look at all bugs, we might as well clear the cache.
    # This also removes bugs that were reassigned. Ideally _update_bugs would
    # know which source package(s) the bug was in, so it could remove the bugs
    # even in the case they have been reassigned to a different package.
    %$c_bts = ();

  }
  else {

    @packages = grep { defined $p{packages}->{ $_ }{trunk} } keys %{ $p{packages} };

    unless ($p{force}) {
      # look only at outdated packages
      @packages = grep { my $ts = $c_bts->{ $_ }->{_pet_timestamp}; ! defined $ts || $now > $ts + $self->timeout } @packages;
    }

  }
  return unless scalar @packages;


  $log->info("Gathering bugs for ", scalar @packages, " packages");

  my @bugs;

  # update open bugs
  my $bugs = $self->_bts->get_bugs(src => \@packages)->result();
  push @bugs, @$bugs;

  # and all other bugs we know about (they might have been closed)
  for my $package (@packages) {
    push @bugs, keys %{ $c_bts->{ $package }->{bugs} || {} };
  }

  @bugs = uniq(@bugs);
  $self->_update_bugs(@bugs);

  for my $package (@packages) {
    $c_bts->{ $package }->{_pet_timestamp} = $now;
    $c_bts->{ $package }->{link}           = $self->package_link_base . $package;
  }
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::Gatherer::BTS - gather packages' bug information from DebBugs

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
