package Debian::PET::Processor::Misc;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use Debian::PET;
use Debian::PET::Version qw( max_version );
use Dpkg::Version qw( version_compare );
use List::MoreUtils qw( none );
use Log::Log4perl;

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub cache {
  Debian::PET->instance->cache;
}

sub _process_package {
  my ($self, $package, $caches) = @_;

  my $archive = $caches->{archive}->{$package};
  my $bts     = $caches->{    bts}->{$package};
  my $vcs     = $caches->{    vcs}->{$package};

  my @archive_versions = values %{ $archive->{version} || [] };

  # XXX: "|| 0" everywhere is ugly
  my $archive_newer    = version_compare($archive->{highest_version} || 0, $vcs->{version} || 0) > 0;
  my $in_archive       = ($vcs->{version} || 0) eq ($archive->{version} || 0);
  my $new_package      = @archive_versions == 0;
  my $work_in_progress = ($vcs->{highest_version} || 0) ne ($vcs->{version} || 0);
  my $ready_for_upload = ($vcs->{distribution} || 0) ne 'UNRELEASED' && $work_in_progress;

  my %info = (
    archive_newer    => $archive_newer,
    in_archive       => $in_archive,
    new_package      => $new_package,
    ready_for_upload => $ready_for_upload,
    work_in_progress => $work_in_progress,
  );

  $caches->{processed}->{$package} = \%info;
}

sub process {
  my ($self, %p) = validated_hash(\@_,
    all_packages => { isa => 'ArrayRef', },
  );

  $log->info("Processing ", scalar @{ $p{packages} }, " packages...");

  my %caches;
  for ("archive", "bts", "control", "processed", "timestamps", "vcs") {
    $caches{ $_ } = $self->cache->read($_);
  }

  for my $package (@{ $p{packages} }) {
    $self->_process_package($package, \%caches);
  }
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::Processor::Misc - needs a better name and cleanup ;)

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
