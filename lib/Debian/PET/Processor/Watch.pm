package Debian::PET::Processor::Watch;

use Moose;
use MooseX::StrictConstructor;
use MooseX::Params::Validate;
use namespace::autoclean;

use Debian::PET;
use Debian::PET::Config qw( config );
use Log::Log4perl;

has 'name' => (
  is       => 'rw',
  isa      => 'Str',
  default  => 'watch',
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub process {
  my ($self, %p) = validated_hash(\@_,
    all_packages => { isa => 'ArrayRef' },
  );

  my $cache = Debian::PET->instance->cache;
  my $c_watch        = $cache->read("watch");
  my $c_consolidated = $cache->read("consolidated");

  $log->info("Processing VCS information for ", scalar @{ $p{all_packages} }, " packages.");

  for my $package (@{ $p{all_packages} }) {
    my $info = $c_consolidated->{ $package }->{watch} = $c_watch->{$package};
  }
}

1;

__END__

=head1 NAME

Debian::PET::Processor::Watch - process watch status

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
