package Debian::PET::Processor::Archive;

use Moose;
use MooseX::StrictConstructor;
use MooseX::Params::Validate;
use namespace::autoclean;

use Debian::PET;
use Dpkg::Version qw( version_compare );
use List::MoreUtils qw( any );
use Log::Log4perl;

has 'name' => (
  is       => 'rw',
  isa      => 'Str',
  default  => 'debian',
);

has 'archive' => (
  is       => 'rw',
  isa      => 'Str',
  default  => 'archive.debian',
);

has '_shared' => (
  is       => 'rw',
  isa      => 'Debian::PET::SharedData::Archive',
  lazy     => 1,
  builder  => '_shared_builder',
  init_arg => undef,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _shared_builder {
  my $self = shift;
  Debian::PET->instance->shared_data($self->archive);
}

sub process {
  my ($self, %p) = validated_hash(\@_,
    all_packages => { isa => 'ArrayRef' },
  );

  my $shared = $self->_shared;

  my $cache = Debian::PET->instance->cache;
  my $c_consolidated = $cache->read("consolidated");
  my $c_vcs = $cache->read("vcs");

  $log->info("Processing archive information for ", scalar @{ $p{all_packages} }, " packages.");

  no warnings "uninitialized";
  for my $package (@{ $p{all_packages} }) {
    my $i_archive = $c_consolidated->{ $package }->{archive}->{ $self->name } = $shared->package($package);
    my $i_vcs = $c_vcs->{ $package };

    $i_archive->{tag_is_higher} = defined $i_vcs->{highest_version} && defined $i_archive->{highest_version} && version_compare($i_vcs->{highest_version}, $i_archive->{highest_version}) > 0;
    $i_archive->{tag_is_lower} = defined $i_vcs->{highest_version} && defined $i_archive->{highest_version} && version_compare($i_vcs->{highest_version}, $i_archive->{highest_version}) < 0;
    $i_archive->{trunk_is_lower} = defined $i_vcs->{version} && defined $i_archive->{highest_version} && version_compare($i_vcs->{version}, $i_archive->{highest_version}) < 0;
    $i_archive->{trunk_in_archive} = any { $_ eq $i_vcs->{version} } values %{ $i_archive->{version} || {} };
  }
}

1;

__END__

=head1 NAME

Debian::PET::Processor::Archive - process archive status

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
