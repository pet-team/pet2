package Debian::PET::Processor::BTS;

use Moose;
use MooseX::StrictConstructor;
use MooseX::Params::Validate;
use namespace::autoclean;

use Debian::PET;
use List::MoreUtils qw( any uniq );
use Log::Log4perl;

has 'name' => (
  is       => 'rw',
  isa      => 'Str',
  default  => 'debian',
);

has 'bugs' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'bugs.debian',
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _has_rc_bugs {
  my $bugs = shift or return 0;
  return any { my $s = $_->{severity}; defined $s && ($s eq 'critical' || $s eq 'grave' || $s eq 'serious') } values %$bugs;
}

sub _all_tags {
  my $bugs = shift or return ();
  return uniq map @{ $_->{tags} || [] }, values %$bugs;
}

sub process {
  my $self = shift;

  my $cache = Debian::PET->instance->cache;
  my $c_bts          = $cache->read($self->bugs);
  my $c_consolidated = $cache->read("consolidated");

  $log->info("Processing BTS information for ", scalar keys %$c_bts, " packages");

  for my $package (keys %$c_bts) {
    my $info = $c_consolidated->{ $package }->{bugs}->{ $self->name } = $c_bts->{ $package };
    $info->{has_rc_bugs} = _has_rc_bugs($info->{bugs});
    $info->{tags}        = [ _all_tags($info->{bugs}) ];
  }
}

1;

__END__

=head1 NAME

Debian::PET::Processor::BTS - process bug status

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
