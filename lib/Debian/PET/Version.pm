package Debian::PET::Version;

use Exporter 'import';

use Dpkg::Version qw( version_compare );
use List::Util qw( reduce );

our @EXPORT_OK = qw( max_version min_version );

sub max_version(@);
sub min_version(@);

sub max_version(@) {
  reduce { version_compare($a, $b) > 0 ? $a : $b } @_;
}

sub min_version(@) {
  reduce { version_compare($a, $b) > 0 ? $b : $a } @_;
}

1;

__END__

=head1 NAME

Debian::PET::Version - utility functions for version handling

=head1 FUNCTIONS

=head2 C<max_version(@versions)>

Return the highest version.

=head2 C<min_version(@versions)>

Return the lowest version.

=head1 SEE ALSO

L<Dpkg::Version>

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
