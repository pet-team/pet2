package Debian::PET::SharedData::CPAN;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use Debian::PET::Cache;
use Debian::PET::Config qw( config );
use Debian::PET::UserAgent;
use IO::Uncompress::AnyUncompress qw( $AnyUncompressError );
use Log::Log4perl;
use Set::Object;
use URI;

has 'name' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'cpan',
);

has 'mirror' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'ftp://ftp.cs.uu.nl/pub/CPAN',
);

has '_agent' => (
  is       => 'rw',
  isa      => 'Debian::PET::UserAgent',
  lazy     => 1,
  builder  => '_agent_builder',
  init_arg => undef,
);

has '_cache' => (
  is       => 'ro',
  isa      => 'Debian::PET::Cache',
  lazy     => 1,
  builder  => '_cache_builder',
  init_arg => undef,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _agent_builder {
  Debian::PET::UserAgent->new;
}

sub _cache_builder {
  my $self = shift;
  Debian::PET::Cache->new(directory => config->{cache_shared});
}

sub _cache_name {
  my $self = shift;
  return $self->name . ".cache";
}

sub _get_filename {
  my ($self, $filename) = @_;
  return File::Spec->catfile(
    config->{cache_shared},
    $self->name . "_" . $filename,
  );
}

sub _download {
  my ($self, $url, $filename) = @_;

  my $response = $self->_agent->mirror($url, $self->_get_filename($filename));

  unless ($response->is_success || $response->code == 304) {
    $log->error("Downloading $url failed: ", $response->status_line);
    return 0;
  }

  1;
}

sub _open_file {
  my ($self, $filename) = @_;

  my $fh = IO::Uncompress::AnyUncompress->new($self->_get_filename($filename));

  unless (defined $fh) {
    $log->error("Opening $filename failed: ", $AnyUncompressError);
    return undef;
  }

  return $fh;
}

sub _update_dist_cache {
  my ($self, $cache) = @_;
  my $fh   = $self->_open_file("02packages.details.txt.gz") or return 0;

  local $_;

  # skip header
  while (<$fh>) {
    last if /^$/;
  }

  # AAC::Pvoice::Row  1.05  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
  my %dists = ();
  while (<$fh>) {
    chomp;
    my (undef, undef, $dist) = split(/\s+/, $_, 3);
    $dists{ $dist } = 1;
  }

  close $fh;

  $log->debug("Found ", scalar keys %dists, " distribtions");

  $cache->{dists} = [ keys %dists ];
  return 1;
}

sub _update_ls_cache {
  my ($self, $cache) = @_;
  my $fh = $self->_open_file("ls-lR.gz") or return 0;

  local $_;

  my @files;

  my ($dir, $dir_of_interest);
  while (<$fh>) {
    chomp;

    if ($_ eq "") {
      $dir = undef;
      $dir_of_interest = undef;
      next;
    }

    if (/^(.*):$/) {
      $dir = $1;
      $dir_of_interest = $dir =~ m{authors/id|modules/by-module};
      next;
    }
    next unless $dir_of_interest;

    # -rw-rw-r--   1 jhi      cpan-adm    1129 Aug 10  1998 README
    # lrwxrwxrwx   1 root     csc           24 Feb 25 03:20 CA-97.17.sperl -> ../../5.0/CA-97.17.sperl
    my @info = split /\s+/;
    next if @info < 9;

    if (! defined $dir) {
      $log->warn("File $info[8] found outside of a directory");
      next;
    }
    next unless $info[8] =~ /\.tar\.gz$/;

    push @files, "$dir/$info[8]";
  }

  close $fh;

  $log->debug("Found ", scalar @files, " files");

  $cache->{files} = \@files;
  return 1;
}

sub update {
  my $self = shift;

  my $cache = {};

  $self->_download(
    $self->mirror . "/modules/02packages.details.txt.gz",
    "02packages.details.txt.gz",
  ) && $self->_update_dist_cache($cache);

  $self->_download(
    $self->mirror . "/indices/ls-lR.gz",
    "ls-lR.gz",
  ) && $self->_update_ls_cache($cache);

  $self->_cache->write($self->_cache_name, $cache);
}

sub watch {
  my ($self, $homepage, $pattern) = @_;
  my $cache = $self->_cache->read($self->_cache_name);

  if (ref($pattern) ne 'Regexp') {
    $pattern = qr/$pattern/;
  }

  my $candidates;
  if ($homepage =~ m{/dist/}) {
    $candidates = $cache->{dists};
  }
  elsif ($homepage =~ m{/authors/id/|/modules/by-module/}) {
    $candidates = $cache->{files};
  }
  else {
    return "NotCPAN";
  }

  if (! @{ $candidates || [] }) {
    return "CPANCacheEmpty";
  }

  my %versions;
  for my $candidate (@$candidates) {
    my @parts = $candidate =~ $pattern or next;
    my $download_link = URI->new_abs($candidate, $homepage);
    my $version = join(".", @parts);
    $versions{ $version } = "$download_link";
  }

  return (undef, \%versions);
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::SharedDate::CPAN - shared information on CPAN releases

=head1 ATTRIBUTES

=head2 name

Name of this shared data handler.

=head2 mirror

URL of a CPAN mirror.

=head1 METHODS

=head2 update

Check for new indices for CPAN, download them if they have changed and re-build
the internal cache.

=head2 watch($homepage, $pattern)

TODO: document

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
