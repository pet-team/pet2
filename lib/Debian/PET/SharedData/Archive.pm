package Debian::PET::SharedData::Archive;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use Debian::PET::Cache;
use Debian::PET::Config qw( config );
use Debian::PET::UserAgent;
use Debian::PET::Version qw( max_version );
use Dpkg::Version qw( version_compare );
use IO::Uncompress::AnyUncompress qw( $AnyUncompressError );
use Log::Log4perl;
use Parse::DebControl;

has 'name' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'debian',
);

has 'mirror' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'http://ftp.debian.org/debian',
);

has 'link_base' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'http://packages.qa.debian.org/',
);

has 'suites' => (
  is      => 'rw',
  isa     => 'ArrayRef[Str]',
  default => sub { [qw( stable testing unstable experimental )] },
);

has 'components' => (
  is      => 'rw',
  isa     => 'ArrayRef[Str]',
  default => sub { [qw( main contrib non-free )] },
);

has 'compression' => (
  is      => 'rw',
  isa     => 'Str',
  default => 'gz',
);

has '_agent' => (
  is       => 'rw',
  isa      => 'Debian::PET::UserAgent',
  lazy     => 1,
  builder  => '_agent_builder',
  init_arg => undef,
);

has '_cache' => (
  is       => 'rw',
  isa      => 'Debian::PET::Cache',
  lazy     => 1,
  builder  => '_cache_builder',
  init_arg => undef,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _agent_builder {
  Debian::PET::UserAgent->new;
}

sub _cache_builder {
  my $self = shift;
  Debian::PET::Cache->new(directory => config->{cache_shared});
}

sub _cache_name {
  my $self = shift;
  return $self->name . ".cache";
}

sub _get_filename {
  my ($self, $suite, $component) = @_;
  return File::Spec->catfile(
    config->{cache_shared},
    $self->name . "_${suite}_${component}_Sources." . $self->compression,
  );
}

sub _get_url {
  my ($self, $suite, $component) = @_;
  return $self->mirror . "/dists/$suite/$component/source/Sources." . $self->compression;
}

sub _download_sources {
  my ($self, $suite, $component) = @_;

  my $url      = $self->_get_url($suite, $component);
  my $filename = $self->_get_filename($suite, $component);
  my $response = $self->_agent->mirror($url, $filename);

  unless ($response->is_success || $response->code == 304) {
    $log->error("Downloading $url failed: ", $response->status_line);
  }
}

sub _open_sources {
  my ($self, $suite, $component) = @_;

  my $filename = $self->_get_filename($suite, $component);
  my $fh = IO::Uncompress::AnyUncompress->new($filename);

  unless (defined $fh) {
    $log->error("Opening $filename failed: ", $AnyUncompressError);
    return undef;
  }

  return $fh;
}

sub _update_cache {
  my ($self, $suite, $component, $cache) = @_;
  my $fh = $self->_open_sources($suite, $component) or return 0;

  my $parser = Parse::DebControl->new;
  # XXX: We use a private function here. Should be made public?
  my $sources = $parser->_parseDataHandle($fh);

  unless (defined $sources) {
    $log->error($self->name, ": Failed to parse Sources for $suite ($component)");
    return 0;
  }

  for my $source (@$sources) {
    my $package = $source->{Package};
    my $version = $source->{Version};
    my $entry   = $cache->{ $package } ||= {};

    my $version_info = $entry->{version} ||= {};
    next if exists $version_info->{ $suite } && version_compare($version_info->{ $suite }, $version) > 0;
    # We are higher than other versions from this suite
    $version_info->{ $suite } = $version;

    next if exists $entry->{highest_version} && version_compare($entry->{highest_version}, $version) > 0;
    # We are the highest version. Also set maintainer information.
    $entry->{highest_version} = $version;
    $entry->{maintainer} = $source->{Maintainer};
  }

  close $fh;
  return 1;
}

sub update {
  my $self = shift;

  my $cache = {};

  for my $suite (@{ $self->suites }) {
    for my $component (@{ $self->components }) {
      $log->info($self->name, ": Updating archive information for $suite ($component)");
      $self->_download_sources($suite, $component);
      $self->_update_cache($suite, $component, $cache);
    }
  }

  while (my ($package, $info) = each %$cache) {
    $info->{link} = $self->link_base . $package;
  }

  $self->_cache->write($self->_cache_name, $cache);
}

sub package {
  my ($self, $package) = @_;
  return $self->_cache->read($self->_cache_name)->{ $package };
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::SharedData::Archive - shared access to repository data

=head1 ATTRIBUTES

=head2 name

Name of this shared data handler.

=head2 mirror

Which site to retrieve the archive information from.

=head2 suites

ArrayRef of suite names to retrieve information for.

=head2 components

ArrayRef of component names to retrieve information for.

=head2 compression

Type of compression used when retrieving Sources list.

=head2 link_base

Base URL used to construct links to a package information package by appending
the name of a source package.

=head1 METHODS

=head2 update

Update Sources files from archive if they have changed and re-build internal
cache from this data.

=head2 package($package)

Get a HashRef with information about the source package C<$package> or C<undef>
if the package is not known.

Currently used keys in the returned hash are

=over

=item highest_version

Highest version of this package.

=item maintainer

Value of the Maintainer field as used in the highest-versioned entry.

=item version

HashMap mapping suite names to the version of this package in that suite.

=back

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
