package Debian::PET::VCS;

use Moose::Role;
use namespace::autoclean;

1;

__END__

=head1 NAME

Debian::PET::VCS - role for version control systems

=head1 DESCRIPTION

The Package Entrophy Tracker can be used with several version control systems.

=head1 METHODS

=head2 get_file

Get the contents of a file out of the VCS.
If the file cannot be found, C<undef> is returned.

=over

=item package => $package

(Required.) Name of the package.

=item file => $file

(Required.) Name of the file.

=item branch => $branch

=item tag => $tag

(Optional.) Fetch file from branch (or tag) instead of trunk.
It is an error to give both branch and tag.

=back

=head2 packages

Returns a list of packages available.

=head2 tags(package => $package)

Returns a list of tags for C<$package>.

=head2 updated_packages

=head2 link

=head1 SEE ALSO

L<Debian::PET::VCS::GitRemote>, L<Debian::PET::VCS::Subversion>

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
