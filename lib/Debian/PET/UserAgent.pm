package Debian::PET::UserAgent;

use strict;
use warnings;

use parent 'LWP::UserAgent';

use Log::Log4perl;

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub new {
  my $class = shift;
  my %opts  = @_;
  my $self  = $class->SUPER::new(%opts);

  # XXX: include version?
  unless (exists $opts{agent}) {
    if (defined $Debian::PET::UserAgent::VERSION) {
      $self->agent("Debian::PET/$Debian::PET::UserAgent::VERSION ");
    }
    else {
      $self->agent("Debian::PET");
    }
  }
  unless (exists $opts{timeout}) {
    $self->timeout(10);
  }
  $self->env_proxy;

  bless $self, $class;
}

sub mirror {
  my ($self, $url, $filename) = @_;
  my $tmp = $filename . ".tmp";

  if (! -f $filename) {
    $log->debug("File $filename not present, downloading from $url");

    my $response = $self->get($url, ':content_file' => $tmp);
    if ($response->is_success) {
      rename $tmp, $filename or die "Could not rename $tmp to $filename: $!";
    }
    return $response;
  }

  my ($size, $mtime) = (stat(_))[7,9];

  my $response = $self->head($url);
  if (! $response->is_success || $response->last_modified > $mtime || $response->content_length != $size) {

    $log->debug("File $filename outdated, downloading from $url");

    $response = $self->get($url, ':content_file' => $tmp);
    if ($response->is_success) {
      rename $tmp, $filename or die "Could not rename $tmp to $filename: $!";
    }
    return $response;

  }

  $log->debug("File $filename has not changed");
  return $response;
}

1;

__END__

=head1 NAME

Debian::PET::UserAgent - Web user agent class

=head1 SEE ALSO

L<LWP::UserAgent>

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
