package Debian::PET::Cache;

# no Moose: also used by the web interface
use strict;
use warnings;

use File::Spec ();
use Storable   ();

sub new {
  my $class = shift;
  my %self  = @_;

  unless (exists $self{directory} && -d $self{directory}) {
    die "The 'directory' parameter is required and must be the name of a directory.";
  }

  bless \%self, $class;
}

sub dump {
  my ($self, $name, $fh, @keys) = @_;
  $fh = \*STDOUT unless defined $fh;

  my $data = $self->read($name);

  if (@keys) {
    my %data;
    $data{$_} = $data->{$_} for (@keys);
    $data = \%data;
  }

  require JSON;
  print $fh JSON::to_json($data, { pretty => 1 });
}

sub read {
  my ($self, $name) = @_;

  unless (exists $self->{_data}->{$name}) {
    my $f = File::Spec->catfile($self->{directory}, $name);
    $self->{_data}->{$name} = -f $f ? Storable::retrieve($f) : {};
  }

  return $self->{_data}->{$name};
}

sub write {
  my ($self, $name, $data) = @_;

  $self->{_data}->{$name} = $data if defined $data;

  unless (exists $self->{_data}->{$name}) {
    die "Tried to write unknown cache '$name'. The 'data' parameter is required if this cache has not been read before.";
  }

  my $filename = File::Spec->catfile($self->{directory}, $name);
  Storable::store($self->{_data}->{$name}, "$filename.tmp");
  rename "$filename.tmp", $filename
    or die "Could not rename $filename.tmp to $filename: $!";
}

sub write_all {
  my $self = shift;
  while (my ($name, $data) = each %{ $self->{_data} }) {
    $self->write($name, $data);
  }
}

1;

__END__

=head1 NAME

Debian::PET::Cache - access cached information

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
