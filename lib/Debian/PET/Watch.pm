package Debian::PET::Watch;

use 5.010;
use strict;
use warnings;

use Exporter 'import';

use Log::Log4perl;

our @EXPORT_OK = qw( parse_watch );

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub parse_watch {
  my $watch_file = shift;
  local $_;

  my $fh;
  if (ref($watch_file)) {
    $fh = $watch_file;
  }
  else {
    open $fh, "<", \$watch_file;
  }

  my $version;
  while (<$fh>) {
    /^version=(\d+)\s*$/ or next;
    $version = $1;
    last;
  }
  return undef unless defined $version;

  # we only support version 2 and 3
  return undef unless $version eq "2" || $version eq "3";

  my @watches;
  my $line = "";
  while (<$fh>) {
    chomp;
    next if /^\s*#/;

    if (/^(.*)\\$/) {
      $line .= $1;
      next;
    }
    else {
      $line .= $_;
    }

    push @watches, _parse_watch_line($line) unless $line =~ /^\s*$/;
    $line = "";
  }

  close $fh unless ref($watch_file);

  return {
    version => $version,
    watches => \@watches,
  };
}

sub _parse_watch_line {
  my $line = shift;
  my $orig = $line;

  my %options;
  if ($line =~ s/^opt(?:ion)?s=//) {
    my $options;
    if ($line =~ s/^"([^"]*)"\s+// || $line =~ s/^(\S+)\s+//) {
      $options = $1;
    }
    else {
      $log->warn("Could not parse options from watch file: '$orig'");
      return undef;
    }

    my @options = split /,/, $options;
    for my $o (@options) {
      my ($key, $value) = split /=/, $o, 2;
      given ($key) {
        when (["active", "passive"])
	  { $options{ $key } = $value; }
	when (/mangle$/)
	  { $options{ $key } = [ split /;/, $value ]; }
	default
	  { $log->warn("Unknown option '$o' ignored."); }
      }
    }
  }

  my ($homepage, $pattern, $version, $action) = split /\s+/, $line, 4;

  # When the homepage contains parentheses in the last component use that as pattern.
  # In that case the line has only three (not four) fields so we have to split again.
  if ($homepage =~ s{/([^/]*\([^/]+\)[^/]*)$}{}) {
    $pattern = $1;
    (undef, $version, $action) = split /\s+/, $line, 3;
  }

  return {
    options  => \%options,
    homepage => $homepage,
    pattern  => $pattern,
    version  => $version,
    action   => $action,
  };
}

1;

__END__

=head1 NAME

Debian::PET::Watch - utility functions for handling watches

=head1 FUNCTIONS

=head2 parse_watch($contents)

=head1 INTERNAL FUNCTIONS

=head2 _parse_watch_line($line)

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
