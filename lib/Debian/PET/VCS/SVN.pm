package Debian::PET::VCS::SVN;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use List::Util qw( max );
use Log::Log4perl;
use Set::Object;
use SVN::Client;

with 'Debian::PET::VCS';

has 'url' => (
  is       => 'rw',
  isa      => 'Str',
  required => 1,
);

has 'name' => (
  is       => 'rw',
  isa      => 'Str',
  required => 1,
);

has 'viewsvn_url' => (
  is       => 'rw',
  isa      => 'Str',
);

has '_ls_cache' => (
  is       => 'rw',
  isa      => 'HashRef',
  default  => sub { {} },
  init_arg => undef,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _client {
  # Do not cache. It leaks all over the place.
  SVN::Client->new;
}

sub get_file {
  my ($self, %p) = validated_hash(\@_,
    package => { isa => 'Str', },
    file    => { isa => 'Str', },

    branch  => { isa => 'Str', optional => 1, },
    tag     => { isa => 'Str', optional => 1, },
  );

  my $url;
  if (exists $p{branch} && exists $p{tag}) {
    $log->logconfess("Parameters branch and tag must not be given at the same time.");
  }
  elsif (exists $p{branch}) {
    $url = $self->url . "/branches/$p{branch}/$p{package}/$p{file}";
  }
  elsif (exists $p{tag}) {
    $url = $self->url . "/tags/$p{package}/$p{tag}/$p{file}";
  }
  else {
    $url = $self->url . "/trunk/$p{package}/$p{file}";
  }

  $log->debug("Retrieving $url");

  my $content;
  open my $fp, ">", \$content
    or $log->logconfess("Could not open handle to in-memory file.");

  local $SVN::Error::handler = undef;
  sleep 1;
  my $result = $self->_client->cat($fp, $self->url . "/trunk/$p{package}/$p{file}", 'HEAD');

  close $fp;

  if (SVN::Error::is_error($result)) {
    if ($result->apr_err() == $SVN::Error::FS_NOT_FOUND) {
      $log->debug("  File not found.");
      return undef;
    }
    $log->error("SVN error while retrieving $url: ", $result->strerror());
    return undef;
  }

  return $content;
}

# returns:
#   HashRef of name -> revision
sub _ls {
  my ($self, $path, $only_directories) = @_;

  if (! $only_directories) {
    $log->logconfess("_ls: only_directories=0 is not supported. SVN breaks if we cache objects...");
  }

  my $file_revs;
  if (exists $self->{_ls_cache}->{ $path }) {
    $file_revs = $self->{_ls_cache}->{ $path };
  }
  else {

    local $SVN::Error::handler = undef;

    my $url = $self->url . $path;
    sleep 1;
    my $files = $self->_client->ls($url, 'HEAD', 0);
    if (SVN::Error::is_error($files)) {
      $log->error("SVN error while retrieving $url: ", $files->strerror());
      return {};
    };

    while (my ($name, $info) = each %$files) {
      # XXX: this must not be done here to support only_directories...
      next if $only_directories && $info->kind != $SVN::Node::dir;
      $file_revs->{$name} = $info->created_rev();
    }

    $self->{_ls_cache}->{ $path } = $file_revs;
    $log->debug($self->url, ": Added $path to cache (", scalar keys %$file_revs, " entries)");
  }

  return $file_revs;
}

sub packages {
  my ($self) = @_;

  $log->debug("Retrieving list of packages for ", $self->url);

  my $packages = $self->_ls('/trunk', 1);

  $log->debug("  found ", scalar keys %$packages, " packages.");

  return $packages;
}

sub trunk {
  my ($self, %p) = validated_hash(\@_,
    package => { isa => 'Str' },
  );

  my $trunk = $self->_ls("/trunk", 1);

  return $trunk->{ $p{package} };
}

sub tags {
  my ($self, %p) = validated_hash(\@_,
    package => { isa => 'Str', },
  );

  my $tags = $self->_ls("/tags/$p{package}", 1);

  return $tags;
}

sub _changed_in_path {
  my ($self, $path, $old, $only_directories) = @_;
  my $new = $self->_ls($path, $only_directories);

  my %diff;

  # deleted in new
  for my $o (keys %$old) {
    next if exists $new->{$o};
    $diff{$o} = undef;
  }

  # other changes
  while (my ($n, $v) = each %$new) {
    next if exists $old->{$n} && $old->{$n} >= $v;
    $diff{$n} = $v;
  }

  return \%diff;
}

# BUG: does not handle packages that were removed
sub changed_packages {
  my ($self, %p) = validated_hash(\@_,
    packages => { isa => 'HashRef' },
  );

  $log->debug("Looking for changes in ", scalar keys %{ $p{packages} }, " packages");

  my %svn = (
    #branches => $self->_ls('/branches', 1),
    tags     => $self->_ls('/tags',     1),
    trunk    => $self->_ls('/trunk',    1),
  );

  $log->debug("Found ", scalar keys %{ $svn{trunk} }, " packages in trunk.");

  my %modified;

  while (my ($name, $status) = each %{ $p{packages} }) {
    
    my %changed_status;

    if (! defined $status->{trunk} || ($svn{trunk}->{ $name } || -1) != $status->{trunk}) {
      $changed_status{trunk} = $svn{trunk}->{ $name };
    }

    # TODO: add support for branches
    $changed_status{ branches } = {};

    #for my $type ("branches", "tags") {
    for my $type ("tags") {

      my $old    = $status->{ $type } || {};
      my $newest = max values %$old;
      # If tags/ directory is empty: use revision from trunk.
      $newest = $status->{trunk} unless defined $newest;

      if (! defined $svn{ $type }->{ $name }) {
        # no tags/branches directory: remove all known tags/branches
        $changed_status{ $type } = { map { $_ => undef } keys %$old };
      }
      elsif (defined $newest && $newest >= $svn{ $type }->{ $name }) {
        # no changes
        $changed_status{ $type } = {};
      }
      else {
        $changed_status{ $type } = $self->_changed_in_path("/$type/$name", $old, 1);
      }

    }

    if (exists $changed_status{trunk} || %{ $changed_status{branches} } || %{ $changed_status{tags} }) {
      $modified{ $name } = \%changed_status;
    }

  }

  $log->debug("Found ", scalar keys %modified, " modified packages.");

  return \%modified;
}

sub link {
  my ($self, %p) = validated_hash(\@_,
    package   => { isa => 'Str' },

    file      => { isa => 'Str', optional => 1 },
    directory => { isa => 'Str', optional => 1 },

    branch    => { isa => 'Str', optional => 1 },
    tag       => { isa => 'Str', optional => 1 },
  );

  my $url = $self->viewsvn_url;

  if (exists $p{branch} && exists $p{tag}) {
    $log->logconfess("Parameters branch and tag must not be given at the same time.");
  }
  elsif (exists $p{branch}) {
    $url .= "/branches/p{branch}/$p{package}";
  }
  elsif (exists $p{tag}) {
    $url .= "/tags/$p{package}/$p{tag}";
  }
  else {
    $url .= "/trunk/$p{package}";
  }

  if (exists $p{file} && exists $p{directory}) {
    $log->logconfess("Parameters file and directory must not be given at the same time.");
  }
  elsif (exists $p{file}) {
    $url .= "/$p{file}?view=markup";
  }
  else {
    $url .= "/" . ($p{directory} || "");
  }

  return $url
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::VCS::SVN - handle access to Subversion repositories

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
