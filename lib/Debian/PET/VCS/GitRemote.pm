package Debian::PET::VCS::GitRemote;

use Moose;
use MooseX::Params::Validate;
use MooseX::StrictConstructor;
use namespace::autoclean;

use JSON 2 qw( decode_json );
use Log::Log4perl;
use Debian::PET::UserAgent;

with 'Debian::PET::VCS';

has 'name' => (
  is       => 'rw',
  isa      => 'Str',
  required => 1,
);

has 'summary_url' => (
  is       => 'rw',
  isa      => 'Str',
  required => 1,
);

has 'gitweb_url' => (
  is       => 'rw',
  isa      => 'Str',
  required => 1,
);

has '_agent' => (
  is       => 'ro',
  isa      => 'LWP::UserAgent',
  lazy     => 1,
  builder  => '_agent_builder',
);

has '_summary' => (
  is       => 'ro',
  isa      => 'HashRef',
  lazy     => 1,
  builder  => '_summary_builder',
  init_arg => undef,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub _agent_builder {
  my ($self) = @_;

  Debian::PET::UserAgent->new;
}

sub _summary_builder {
  my $self = shift;

  $log->debug("Retrieving ", $self->summary_url);

  my $response = $self->_agent->get($self->summary_url);
  unless ($response->is_success) {
    $log->logconfess("Retrieving ", $self->summary_url, " failed: ", $response->status_line);
  }
  my $content = $response->content;

  $log->debug("  read ", length $content, " bytes");

  return decode_json($content);
}

sub get_file {
  my ($self, %p) = validated_hash(\@_,
    package => { isa => 'Str', },
    file    => { isa => 'Str', },

    branch  => { isa => 'Str', optional => 1, },
    tag     => { isa => 'Str', optional => 1, },
  );

  my $url;
  if (exists $p{branch} && exists $p{tag}) {
    $log->logconfess("Parameters branch and tag must not be given at the same time.");
  }
  elsif (exists $p{branch}) {
    # TODO: implement
    $log->logconfess("NOT YET IMPLEMENTED");
  }
  elsif (exists $p{tag}) {
    # TODO: implement
    $log->logconfess("NOT YET IMPLEMENTED");
  }
  else {
    $url = $self->gitweb_url . "/$p{package}.git;a=blob_plain;f=$p{file}";
  }

  $log->debug("Retrieving $url");

  my $response = $self->_agent->get($url);
  sleep 1;
  if ($response->code == 404) {
    $log->debug("  File not found.");
    return undef;
  }
  unless ($response->is_success) {
    $log->logconfess("Retrieving ", $url, " failed: ", $response->status_line);
  }

  return $response->content;
}

sub packages {
  my ($self) = @_;

  my %packages;

  while (my ($package, $status) = each %{ $self->_summary }) {
    $packages{ $package } = $status->{trunk};
  }

  return \%packages;
}

sub tags {
  my ($self, %p) = validated_hash(\@_,
    package => { isa => 'Str', },
  );

  my $summary = $self->_summary;
  unless (exists $summary->{ $p{package} }) {
    $log->logconfess("Package $p{package} not found in summary.");
  }

  # only return tags that belong to Debian releases
  my %tags;
  my $git_tags = $summary->{ $p{package} }->{tags};
  while (my ($tag, $rev) = each %$git_tags) {
    $tag =~ m{debian/(.+)} or next;

    # "~" is invalid in Git tag names so it is mapped to "_" or ".",
    # but we can only restore "_" as "." could also just be a ".".
    # ":" is mapped to "%".
    my $version = $1;
    $version =~ tr/_%/~:/;

    $tags{ $version } = $rev;
  }

  return \%tags;
}

sub trunk {
  my ($self, %p) = validated_hash(\@_,
    package => { isa => 'Str' },
  );

  my $summary = $self->_summary->{ $p{package} };
  unless ($summary) {
    $log->logconfess("Package $p{package} not found in summary.");
  }

  return $summary->{trunk};
}

# get changes to refs
# input:
#   $as: HashRef of ref -> revision
#   $bs: HashRef of ref -> revision
# output:
#   hashref of ref -> revision, where
#     ref is not identical in $as and $bs
#     revision is undef if ref is in $as, but not in $bs, or the value from $bs
sub _changed_refs {
  my ($as, $bs) = @_;

  my %diff;

  # deleted refs
  for my $a (keys %$as) {
    next if exists $bs->{$a};
    $diff{$a} = undef;
  }

  # other changes
  while (my ($b, $v) = each %$bs) {
    next if exists $as->{$b} && $as->{$b} eq $v;
    $diff{$b} = $v;
  }

  return \%diff;
}

# BUG: does not handle packages that were removed from Git
sub changed_packages {
  my ($self, %p) = validated_hash(\@_,
    packages => { isa => 'HashRef' },
  );

  my %modified;

  while (my ($name, $status) = each %{ $p{packages} }) {

    my $summary = $self->_summary->{ $name };
    my %changed_status;

    if (defined $summary) {

      if (! defined $status->{trunk} || $status->{trunk} ne $summary->{trunk}) {
        $changed_status{trunk} = $summary->{trunk};
      }

      $changed_status{tags} = _changed_refs($status->{tags}, $self->tags(package => $name));

      # TODO: add support for branches
      $changed_status{branches} = {};
    }
    else {
      # package no longer in summary: repository has been removed
      $changed_status{trunk} = undef;
      $changed_status{branches} = { map { $_ => undef } keys %{ $status->{branches} } };
      $changed_status{tags} = { map { $_ => undef } keys %{ $status->{tags} } };
    }

    if (exists $changed_status{trunk} || %{ $changed_status{branches} } || %{ $changed_status{tags} }) {
      $modified{ $name } = \%changed_status;
    }
  }

  $log->debug("Found ", scalar keys %modified, " modified packages.");

  return \%modified;
}

sub link {
  my ($self, %p) = validated_hash(\@_,
    package   => { isa => 'Str' },

    file      => { isa => 'Str', optional => 1 },
    directory => { isa => 'Str', optional => 1 },

    branch    => { isa => 'Str', optional => 1 },
    tag       => { isa => 'Str', optional => 1 },
  );

  my $url = $self->gitweb_url . "/$p{package}.git";

  if (exists $p{file} && exists $p{directory}) {
    $log->logconfess("Parameters file and directory must not be given at the same time.");
  }
  elsif (exists $p{file}) {
    $url .= ";a=blob;f=$p{file}";
  }
  elsif (exists $p{directory}) {
    $url .= ";a=free;f=$p{directory}";
  }
  else {
    $url .= ';a=shortlog';
  }

  if (exists $p{branch} && exists $p{tag}) {
    $log->logconfess("Parameters branch and tag must not be given at the same time.");
  }
  elsif (exists $p{branch}) {
    $url .= ";h=refs/heads/$p{branch}";
  }
  elsif (exists $p{tag}) {
    my $tag = $p{tag};
    $tag =~ s/~/_/g;
    $url .= ";h=refs/tags/debian/$tag";
  }

  return $url;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::VCS::GitRemote - handle access to remote Git repositories

=head SEE ALSO

L<pet-git-helper>

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
