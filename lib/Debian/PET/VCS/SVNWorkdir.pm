package Debian::PET::VCS::SVNWorkdir;

use Moose;
use MooseX::StrictConstructor;
use MooseX::Params::Validate;
use namespace::autoclean;

use File::Spec;
use List::MoreUtils qw( any );
use Log::Log4perl;

extends 'Debian::PET::VCS::SVN';

has 'workdir' => (
  is       => 'rw',
  isa      => 'Str',
  required => 1,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

override 'get_file' => sub {
  my ($self, %p) = validated_hash(\@_,
    package => { isa => 'Str' },
    file    => { isa => 'Str' },

    branch  => { isa => 'Str', optional => 1 },
    tag     => { isa => 'Str', optional => 1 },
  );

  if (exists $p{branch} || exists $p{tag}) {
    return super();
  }

  my $file = File::Spec->catfile($self->workdir, $p{package}, $p{file});
  if (any { $_ eq ".." } File::Spec->splitdir($file)) {
    $log->logconfess("File name constructed to retrieve $p{file} for package $p{package} did contain '..'");
  }

  local $/ = undef;
  open my $fh, "<", File::Spec->catfile($self->workdir, $p{package}, $p{file})
      or return undef;
  my $content = <$fh>;
  close $fh;

  return $content;
};

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET::VCS::SVNWorkdir - handle access to Subversion repositories with a local workdir

=head1 SEE ALSO

L<Debian::PET::VCS::SVN>

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
