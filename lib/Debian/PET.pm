package Debian::PET;
# ABSTRACT: package entrophy tracker

use MooseX::Singleton;
use MooseX::Params::Validate;
use namespace::autoclean;

use Class::MOP;
use Class::MOP::Class;
use Debian::PET::Cache;
use Debian::PET::Config qw( config );
use List::Util qw( min );
use Log::Log4perl;

has 'cache' => (
  is      => 'rw',
  isa     => 'Debian::PET::Cache',
  lazy    => 1,
  builder => '_cache_builder',
);

has '_vcs' => (
  is       => 'rw',
  isa      => 'HashRef',
  default  => sub { {} },
  init_arg => undef,
);

my $log = Log::Log4perl->get_logger(__PACKAGE__);

sub BUILD {
  Log::Log4perl::init(config->{log});
  $log->info("Debian::PET", defined $Debian::PET::VERSION ? " v$Debian::PET::VERSION" : "", " starting");
}

sub _cache_builder {
  my $self = shift;
  Debian::PET::Cache->new(directory => config->{cache});
}

sub _run_gatherers {
  my ($self, %p) = validated_hash(\@_,
    gatherers => { isa => 'ArrayRef', optional => 1 },
    packages  => { isa => 'HashRef', optional => 1 },
    force     => { isa => 'Bool', optional => 1 },
    force_all => { isa => 'Bool', optional => 1 },
  );

  my @all_packages = keys %{ $self->cache->read("vcs") };

  my $gatherers = $p{gatherers} || config->{on_update}->{gather} || [];
  $gatherers = [ $gatherers ] unless ref($gatherers) eq 'ARRAY';
  delete $p{gatherers};

  for my $name (@$gatherers) {

    my $gatherer = $self->gatherer($name);
    $gatherer->gather(%p, all_packages => \@all_packages);

    # VCS gatherer might have found new packages
    if ($name eq 'VCS') {
      @all_packages = keys %{ $self->cache->read("vcs") };
    }

  }
}

sub update {
  my ($self, %p) = validated_hash(\@_,
    vcs       => { isa => 'ArrayRef[Str]', optional => 1 },
    force     => { isa => 'Bool', optional => 1 },
    force_all => { isa => 'Bool', optional => 1 },
    limit     => { isa => 'Int', default => 100 },
  );
  my @vcses = $p{vcs} ? @{ $p{vcs} } : $self->vcses;

  my $vcs_cache = $self->cache->read("vcs");

  my %changed;
  my $count = 0;
  for my $name (@vcses) {

    my $vcs = $self->vcs($name);

    my %packages;
    if (exists $p{packages}) {
      $log->logconfess("NOT YET IMPLEMENTED.");
    }
    else {
      while (my ($package, $status) = each %$vcs_cache) {
        next unless $status->{vcs} eq $name;
        $packages{ $package } = $status;
      }

      # handle NEW packages
      for my $p (keys %{ $vcs->packages }) {

        # check before we add packages to handle limit=0 correctly
        last if $count >= $p{limit};

        next if exists $packages{ $p };
        $packages{ $p } = {
          trunk    => undef,
          branches => {},
          tags     => {},
        };
        $count++;
      }
    }

    next unless %packages;
    $changed{ $name } = $vcs->changed_packages(packages => \%packages);
    $changed{ $name }->{ $_ }{vcs} = $name for keys %{ $changed{ $name } };

  }

  # handle REMOVE and ADD in one transaction
  # But Debian::PET::VCS::* need support for it first...
  my %merged;
  for my $packages (values %changed) {
    while (my ($package, $status) = each %$packages) {
      $merged{ $package } = $status;
    }
  }

  my $gatherers = config->{on_update}->{gather} || [];
  $gatherers = [ $gatherers ] unless ref($gatherers) eq 'ARRAY';

  $self->_run_gatherers(
    gatherers    => [ 'VCS' ],
    packages     => \%merged,
    $p{force}     ? (force     => $p{force})     : (),
    $p{force_all} ? (force_all => $p{force_all}) : (),
  );
  $self->_run_gatherers(
    gatherers    => [ grep { $_ ne 'VCS' } @$gatherers ],
    packages     => \%merged,
    $p{force_all} ? (force     => $p{force})     : (force => exists $p{force} ? $p{force} : 1),
    $p{force_all} ? (force_all => $p{force_all}) : (),
  );
}

sub _cmd_update {
  my ($self, $params, $arguments) = @_;

  $self->update(
    @$arguments ? (vcs => $arguments) : (),
    %$params,
  );
}

sub _cmd_cron {
  my ($self, $params, $arguments) = @_;
  my %p = %$params;

  $self->_run_gatherers(
    $p{force}     ? (force     => $p{force})     : (),
    $p{force_all} ? (force_all => $p{force_all}) : (),
  );
}

sub _cmd_process {
  my ($self, $params, $arguments) = @_;

  my $c_vcs = $self->cache->read("vcs");
  my @packages = keys %{ $c_vcs };

  my @processors = @$arguments ? @$arguments : $self->processors;
  for my $name (@processors) {
    my $processor = $self->processor($name);
    $processor->process(all_packages => \@packages, %$params);
  }

  # We are only interested in packages that exist in the VCS.
  # Other packages might be leftovers in some cache.
  my $c_consolidated = $self->cache->read("consolidated");
  for my $package (keys %{ $c_consolidated }) {
    $c_vcs->{ $package } or delete $c_consolidated->{ $package };
  }
}

sub _cmd_update_shared {
  my ($self, $params, $arguments) = @_;

  for my $name ($self->shared_datas) {
    my $shared_data = $self->shared_data($name);
    $shared_data->update;
  }
}

sub _cmd_remove {
  my ($self, $params, $arguments) = @_;

  my $vcs_status = $self->cache->read("vcs");
  my $count = grep defined $_, delete @$vcs_status{ @$arguments };

  $log->info("Removed $count packages from VCS info hash.");
}

sub _get_instance {
  my ($self, $class, $name, %p) = @_;
  delete $p{type};

  Class::MOP::load_class($class);
  my $meta = Class::MOP::Class->initialize($class);

  unless (defined $meta) {
    $log->logconfess("Instance of non-existing class $class requested");
  }

  return $meta->new_object(name => $name, %p);
}

sub gatherer {
  my ($self, $name) = @_;

  my $p = config->{gatherer}->{ $name };

  unless (defined $p) {
    $log->logconfess("Instance of unknown gatherer $name requested.");
  }

  my $class = "Debian::PET::Gatherer::$p->{type}";
  $self->_get_instance($class, $name, %$p);
}

sub gatherers {
  return keys %{ config->{gatherer} || {} };
}

sub shared_data {
  my ($self, $name) = @_;

  my $p = config->{shared_data}->{ $name };

  unless (defined $p) {
    $log->logconfess("Instance of unknown shared data handler $name requestd.");
  }

  my $class = "Debian::PET::SharedData::$p->{type}";
  $self->_get_instance($class, $name, %$p);
}

sub shared_datas {
  return keys %{ config->{shared_data} || {} };
}

sub processor {
  my ($self, $name) = @_;

  my $p = config->{processor}->{ $name };

  unless (defined $p) {
    $log->logconfess("Instance of unknown processor $name requested.");
  }

  my $class = "Debian::PET::Processor::$p->{type}";
  $self->_get_instance($class, $name, %$p);
}

sub processors {
  return keys %{ config->{processor} || {} };
}

sub vcs {
  my ($self, $name) = @_;

  unless (exists $self->_vcs->{ $name }) {
    my $p = config->{vcs}->{ $name };

    unless (defined $p) {
      $log->logconfess("Instance of unknown VCS $name requested.");
    }

    my $class = "Debian::PET::VCS::$p->{type}";
    $self->_vcs->{ $name } = $self->_get_instance($class, $name, %$p);
  }

  return $self->_vcs->{ $name };
}

sub vcses {
  return keys %{ config->{vcs} || {} };
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 NAME

Debian::PET - Package Entropy Tracker for Debian packages

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
