#! /usr/bin/perl

use strict;
use warnings;

use Safe;

my ($string, $code) = @ARGV;
my $safe = Safe->new;
$safe->permit_only(
  # from :base_core
  qw( const leaveeval lineseq list pushmark ),

  # from :base_orig
  qw( padany subst ),
);

local $_ = $string;
$safe->reval("$code");
die $@ if ($@);
die '$_ is not a scalar.' if ref($_);

print $_, "\n";

