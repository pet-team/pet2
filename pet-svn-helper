#! /usr/bin/perl

use strict;
use warnings;

use LWP::UserAgent;

my $notify_url = $ENV{PET_NOTIFY}
  or fatal("PET_NOTIFY must be an URL to send the notification to.");
my $name = $ENV{PET_SVN_NAME}
  or fatal("PET_SVN_NAME must be the name used by PET for this repository.");

my $agent  = LWP::UserAgent->new;
my $result = $agent->post($notify_url, {
    action     => 'notify',
    repository => $name,
});

unless ($result->is_success) {
  print STDERR "Notification failed: ", $result->status_line, "\n";
  print STDERR $result->decoded_content;
}

exit 0;

###########################################################

sub fatal {
  print STDERR @_, "\n";
  exit 1;
}

__END__

=head1 NAME

pet-svn-helper - send commit notifications

=head1 ENVIRONMENT VARIABLES

=head2 PET_NOTIFY

(Required.) Notify a PET instance of changes by sending a notification to this
URL.

=head2 PET_SVN_NAME

(Required.) Name that PET uses for the SVN repository that is sending a
notificitation.

=head1 SEE ALSO

L<Debian::PET::VCS::SVN>

=head1 AUTHOR AND COPYRIGHT

Copyright 2010, Ansgar Burchardt <ansgar@43-1.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut

# vim:set et sw=2:
